#!/bin/sh
#
# plot.sh
#
# Generate demo plots

URI="postgresql://postgres:${POSTGRES_PASSWORD}@127.0.0.1/${POSTGRES_DB}"

while ! psql $URI < /dev/null; do
  echo "Waiting for database..."
  sleep 1
done

mkdir -p /var/www/html/img


# static.png
psql $URI -c "
	COPY (
		SELECT
			t,
			truth,
			measurement,
			static_filter(measurement) AS estimation
		FROM demo.temperatures
		ORDER BY t
	) TO stdout WITH (FORMAT CSV, HEADER)
" | \
awk 'NR==1 { print; next } { printf "%s,%f,%f,%f\n", $1, $2, $3, $4 }' > /tmp/static.csv

gnuplot -p -e "\
	set terminal png size 800,300 background rgb '#FFFFFF';\
	set key textcolor rgb '#46444D';\
	set output '/var/www/html/img/static.png';\
	set datafile separator ',';\
	set key below autotitle columnhead;\
	set title 'Tank Temperature';\
	set xlabel 'Time';\
	set ylabel 'Degrees Fahrenheit';\
	plot \
	'/tmp/static.csv' using 1:2 with line lw 3 lt rgb '#CCCCCC',\
	'/tmp/static.csv' using 1:3 with points ls 3 lt rgb '#6D9DC2',\
	'/tmp/static.csv' using 1:4 with lines lw 5 lt rgb '#C175BB'"


# dynamic.png
psql $URI -c "
	COPY (
		SELECT
			t,
			truth,
			measurement,
			dynamic_filter(measurement) AS estimation
		FROM demo.ranges
		ORDER BY t
	) TO stdout WITH (FORMAT CSV, HEADER)
" | \
awk 'NR==1 { print; next } { printf "%s,%f,%f,%f\n", $1, $2, $3, $4 }' > /tmp/dynamic.csv

gnuplot -p -e "\
	set terminal png size 800,300 background rgb '#FFFFFF';\
	set key textcolor rgb '#46444D';\
	set output '/var/www/html/img/dynamic.png';\
	set datafile separator ',';\
	set key below autotitle columnhead;\
	set title 'Range to Target';\
	set xlabel 'Time';\
	set ylabel 'Distance';\
	plot \
	'/tmp/dynamic.csv' using 1:2 with line lw 3 lt rgb '#CCCCCC',\
	'/tmp/dynamic.csv' using 1:3 with points ls 3 lt rgb '#6D9DC2',\
	'/tmp/dynamic.csv' using 1:4 with lines lw 5 lt rgb '#C175BB'"


echo "Generated plots"
