#!/bin/sh
#
# welcome.sh
#
# Output a welcome message

URI="postgresql://postgres:${POSTGRES_PASSWORD}@127.0.0.1/${POSTGRES_DB}"

while ! psql $URI < /dev/null; do
  echo "Waiting for database..."
  sleep 1
done
sleep 1

cat <<'EOF'
               _        _
  _ __  __ _  | |____ _| |_ __  __ _ _ _
 | '_ \/ _` | | / / _` | | '  \/ _` | ' \
 | .__/\__, |_|_\_\__,_|_|_|_|_\__,_|_||_|
 |_|   |___/___|

EOF

cat <<EOF
Welcome to the pg_kalman demo!

A webpage is being served at:

  http://127.0.0.1

You can connect to the database with:

  psql $URI

You can stop this demo with:

  ctrl + c

EOF
