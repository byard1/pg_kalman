/*
 * demo.sql - Initialization queries for the demo
 */

CREATE EXTENSION IF NOT EXISTS pg_kalman;
CREATE EXTENSION IF NOT EXISTS tablefunc; -- for normal_rand()

CREATE SCHEMA IF NOT EXISTS demo;

CREATE TABLE demo.temperatures AS (
	SELECT
		t,
		68 AS truth,
		normal_rand(1, 68, 1) AS measurement
	FROM generate_series(1, 60) AS t
);

COMMENT ON TABLE demo.temperatures IS
'noisy temperature readings';


CREATE TABLE demo.ranges AS (
	SELECT
		t,
		pow(60,2) - pow(t,2) AS truth,
		normal_rand(1, pow(60,2) - pow(t,2), 256) AS measurement
	FROM generate_series(1, 60) AS t
);

COMMENT ON TABLE demo.ranges IS
'noisy range to target readings';
