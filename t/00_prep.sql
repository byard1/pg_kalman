CREATE EXTENSION pgtap;
CREATE EXTENSION pg_kalman;
CREATE EXTENSION tablefunc; -- for normal_rand()

SELECT plan(1);
SELECT pass('prep ran');
SELECT * FROM finish();
